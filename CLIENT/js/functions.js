
function hideAllPages() {
    pages.forEach(p => {
        p.classList.add('hide');
    });
}

function showPage(page) {
    pageTransition();
    setTimeout(() => {
        hideAllPages();
    }, 1500);
    setTimeout(() => {
        page.classList.remove('hide');
    }, 1500);
}

function pageTransition() {
    transition.classList.add('active');
    setTimeout(() => {
        transition.classList.remove('active');
    }, 3000);
}