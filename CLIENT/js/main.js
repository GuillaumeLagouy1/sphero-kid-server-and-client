
var socket;

var teamData;

mainAudio.volume = 0.02; 

socket = io.connect('http://192.168.43.146:8083');

socket.on('shape', function(data) {
    setShapePage(data)
});

socket.on('color', function(data) {
    setColorPage(data)
});

socket.on('players', function(data) {
    
    teamData = [];

    data.forEach( function(element) {
        var playerJSON = JSON.parse(element)
        playerJSON.avatar = "data:image/png;base64," + playerJSON.avatar;
        playerJSON.record = "data:image/png;base64," + playerJSON.record;
        teamData.push(playerJSON);
    });
    //displayNames(data);
    setTeamPage(teamData)
});

socket.on('finalDrawing', function(data) {
    setAnimationPage();
})

/*
var colorData = {
    color : 'red',
    duration : 5
}*/

socket.on('spheroDrawing', function(data) {
    showPage(drawing);
})
/*
var shapeData = {
    shape: "circle",
    duration: 5
}

var colorData = {
    color: "red",
    duration: 5
}
*/




//-------NAME MANAGEMENT START 
/*teamData = [
    {firstname: "player1", avatar: avatarBase64, record: recordBase64},
    {firstname: "player2", avatar: avatarBase64, record: recordBase64},
    {firstname: "player3", avatar: avatarBase64, record: recordBase64},
    {firstname: "player4", avatar: avatarBase64, record: recordBase64},
    {firstname: "player5", avatar: avatarBase64, record: recordBase64},
];*/

//setTeamPage(teamData);

//setShapePage(shapeData)

//setColorPage(colorData)

//showPage(drawing)

//showPage(ending)

//setAnimationPage();
//displayColor(aColor);
//displayShape(aShape);
//-------NAME MANAGEMENT END
