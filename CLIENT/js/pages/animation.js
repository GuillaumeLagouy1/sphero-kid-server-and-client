function setAnimationPage() {
    showPage(animation);

    setTimeout(() => {
        var video = document.getElementById('video');
        video.play();
        video.volume = 0.3; 
        mainAudio.pause();
    }, 3000);

    setTimeout(() => {
        showPage(ending);
    }, animationTimout);

}