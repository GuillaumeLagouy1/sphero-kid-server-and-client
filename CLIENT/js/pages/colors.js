
//-------COLOR MANAGEMENT START 
function setColorPage(data) {
    showPage(color);
    //set image
    color.querySelector('#color-image').src = "assets/colors/" + data.color + ".png";

    //timout
    setTimeout(function() {
        setGuessingPageColor(teamData)
        showPage(guessing);
        setTimeout(() => {
            setGuessingAudio(teamData[4].record, teamData[2].record, 'color');
        }, 1000);
    }, timoutGuessing);
}
//-------COLOR MANAGEMENT END 