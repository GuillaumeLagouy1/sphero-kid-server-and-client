
function setGuessingPageColor(data) {

    var players = guessing.querySelectorAll('.player');

    players[0].querySelector('.name').innerHTML = data[4].firstname;
    players[0].querySelector('img').src = data[4].avatar;

    players[1].querySelector('.name').innerHTML = data[2].firstname;
    players[1].querySelector('img').src = data[2].avatar;
    
}

function setGuessingPageShape(data) {
    
    var players = guessing.querySelectorAll('.player');
    
    players[0].querySelector('.name').innerHTML = data[3].firstname;
    players[0].querySelector('img').src = data[3].avatar;

    players[1].querySelector('.name').innerHTML = data[1].firstname;
    players[1].querySelector('img').src = data[1].avatar;

}

function setGuessingAudio(record1, record2, type) {

    var index = 1;
    setTimeout(() => {
        audio.src = record1;
        audio.play();
        
        audio.onended = function() {
            if(index == 1){
                if(type === 'color')
                    audio.src="assets/audio/guess_color.wav";
                else
                    audio.src="assets/audio/guess_shape.wav";
                audio.play();
                index++;
            }
            else if(index == 2) {
                audio.src = record2;
                audio.play();
                index++;
            }
            else {
                index = 1;
            }
        };
    },2700);
}
