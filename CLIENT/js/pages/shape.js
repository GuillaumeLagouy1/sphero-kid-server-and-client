
//-------COLOR MANAGEMENT START 
function setShapePage(data) {
    showPage(shape);
    //set image
    shape.querySelector('#shape-image').src = "assets/shapes/" + data.shape + ".png";
    //timout
    setTimeout(function() {
        setGuessingPageShape(teamData)
        showPage(guessing);
        setTimeout(() => {
            setGuessingAudio(teamData[3].record, teamData[1].record, 'shape');
        }, 1000);
    }, timoutGuessing);
}
//-------COLOR MANAGEMENT END 