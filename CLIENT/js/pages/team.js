
/*function displayNames(data) {
    teamScreen.classList.remove('hide');

    //set list
    var list = document.querySelector('#team .container .players');

    var drawer = document.createElement('span');
    drawer.dataset.theme = 'Dessinateur';

    var shaper = document.createElement('span');
    shaper.dataset.theme = 'Forme';

    var colorer = document.createElement('span');
    colorer.dataset.theme = 'Couleur';

    var firstSpan = document.createElement('span');

    list.appendChild(drawer);
    list.appendChild(shaper);
    list.appendChild(colorer);

    data.forEach((e, i) => {

        var iconSource;

        var item = document.createElement('div')
        item.classList.add('player');

        var name = document.createElement('div')
        name.classList.add('name');
        name.innerHTML = e;

        var icon = document.createElement('img')
        icon.classList.add('icon');
        icon.src = setIcon(i);

        item.appendChild(name);
        item.appendChild(icon)

        if(i == 0)
            drawer.appendChild(item)
        if(i == 1 || i == 2)
            shaper.appendChild(item)
        if(i == 3 || i == 4)
            colorer.appendChild(item)

    })
}

function setIcon(index) {

    var res = '';

    if(index == 0) 		res = 'brush';
    else if(index == 1) res = "eye"
    else if(index == 2) res = "mouth"
    else if(index == 3) res = "eye"
    else 				res = "mouth"

    return "assets/" + res + ".png";
}
*/

function setTeamPage(data) {
    showPage(team);
    data.forEach((el, i) => {
        var div = document.getElementById('player' + (i + 1));
        setPlayer(div, el.firstname, el.avatar)
    })
}

function setPlayer(item, firstname, avatar) {
    item.querySelector('.name').innerHTML = firstname;
    //item.querySelector('.photo-player').src = "assets/avatar.png";
    item.querySelector('.photo-player').src = avatar;
}
