
//-------WAITING SCREEN START
function showWaitingScreen() {
    //set title
    pageTitle.innerHTML = "A vous de jouer !";
    //show / hide
    waitingScreen.classList.remove('hide');
}

function hideWaitingScreen() {
    waitingScreen.classList.add('hide');
}

function hideNamesScreen() {
    namesScreen.classList.add('hide')
}
//-------WAITING SCREEN END