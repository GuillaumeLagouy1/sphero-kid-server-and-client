//-------COMMON VARIABLES START
var pages = document.querySelectorAll('.page');

//PAGES
var home       = document.getElementById('home');
var team       = document.getElementById('team');
var guessing   = document.getElementById('guessing');
var color      = document.getElementById('color');
var shape      = document.getElementById('shape');
var waiting    = document.getElementById('waiting');
var drawing    = document.getElementById('drawing');
var animation  = document.getElementById('animation');
var ending     = document.getElementById('ending');
var transition = document.getElementById('transition');

//COMMON
var mainAudio = document.getElementById('main_audio');
var audio = document.getElementById('audio');
var timoutGuessing = 8000;
var animationTimout = 20000;
//-------COMMON VARIABLES END