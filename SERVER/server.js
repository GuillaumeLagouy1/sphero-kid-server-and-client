var app = require('http').createServer();
var io = require('socket.io')(app);

console.log("My server is running !");

app.listen(8083);

io.on('connection', newConnection);

function newConnection(socket) {
	console.log("New connection : " + socket.id);
	
	
	socket.on('test', function(data) {
		console.log(data)
		socket.broadcast.emit('test', data);
		// même chose ...
		//io.sockets.emit('shape', data);
	})

	socket.on('shape', function(data) {
		console.log(data)
		socket.broadcast.emit('shape', data);
	})

	socket.on('color', function(data) {
		console.log(data)
		socket.broadcast.emit('color', data);
	})

	socket.on('players', function(data) {
		socket.broadcast.emit('players', data);
	})

	socket.on('action', function(data) {
		console.log(data)
		socket.broadcast.emit('action', data)
	})

	socket.on('finalDrawing', function(data) {
		socket.broadcast.emit('finalDrawing', data)
	})

	socket.on('spheroDrawing', function(data) {
		socket.broadcast.emit('spheroDrawing', data)
	})


}